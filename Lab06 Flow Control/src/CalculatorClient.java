import java.util.Scanner;

public class CalculatorClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
         Scanner sc = new Scanner(System.in);
         
         System.out.print("Enter the size of your array: ");
         int num = sc.nextInt();
         int[] newArray = new int[num];
         
         System.out.println("Enter the numbers.");
         
         for(int i=0; i<num; i++) {
        	 newArray[i] = sc.nextInt();
          }
         
         Calculator cal1 = new Calculator();
         int totalSum = cal1.sumNumbers(newArray);
         System.out.println("The sum of your array is: " + totalSum);
         
         Calculator cal2 = new Calculator();
         int arr[] = new int[]{ 5,6,5,10,2,2,3,8,10,10,11}; 
         int n = arr.length; 
         cal2.countArray(arr, n); 
	}

}
