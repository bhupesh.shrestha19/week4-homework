
public class DailyMessageWithEnums {
	
	
	String getMessage(DayOfTheWeek day) {
		
		switch (day) {
		
		case MONDAY:
			return "study day";
			
		case WEDNESDAY:
			return "study day";
			
		case FRIDAY:
			return "study day";
			
		case TUESDAY:
			return "gym day";
			
		case THURSDAY:
			return "gym day";
			
		case SATURDAY:
			return "weekend";
		case SUNDAY:
			return "weekend";
			
		default:
			return "gym day";
		}
	}

}


