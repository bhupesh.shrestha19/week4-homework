/**
 * 
 * @author Bhupesh Shrestha
 * Date: 09/08/2020
 * Lab 6.1a - A Daily Message
 * 
 */

public class DailyMessage {
  
	String getMessage(String day) {
		if (day == "Monday" || day == "Wednesday" || day == "Friday") {
			return "study day";
		}
		else if (day == "Tuesday" || day == "Thursday") {
			return "gym day";
		}
		else if (day == "Saturday" || day == "Sunday") {
			return "weekend";
		}
		else {
			return "invalid day";
		}
	}
}
